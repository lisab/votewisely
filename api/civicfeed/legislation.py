import requests
import json
import traceback
import sys
from os.path import isfile

# For tests
sys.path.insert(0, "api/civicfeed")

from civicfeed_key import key

# Config
HEADERS={'x-api-key': key}
STATES_METADATA_URL = 'https://api-beta.civicfeed.com/legislation/metadata'
STATES_METADATA_FILENAME = 'states_metadata.json'

def get_states_metadata_from_url():
    """Gets and returns the states metadata information on CivicFeed.

    Gets and returns the states metadata information on CivicFeed using a GET request
    to CivicFeed's official API.

    Parameters: None

    Returns:
    dict('success', 'content', 'error'):
        Content of the response from the GET request.
    """
    global STATES_METADATA_URL, HEADERS
    r = requests.get(
            url=STATES_METADATA_URL,
            headers=HEADERS)
    if r.status_code != 200:
        return {'success': False,
                'content': None,
                'error': 'Bad status code: ' + str(r.status_code)}
    elif len(r.headers['content-type']) < 16 or \
            r.headers['content-type'][:16] != 'application/json':
        return {'success': False,
                'content': r.text,
                'error': 'Not JSON content-type: ' + r.headers['content-type']}
    return {'success': True,
            'content': r.json(),
            'error': None}

def get_states_metadata(force_update=False):
    """Gets and returns the states metadata information on CivicFeed.

    Gets and returns the states metadata information on CivicFeed stored locally.

    Parameters:
    force_update (bool): Indicates to update the metadata information locally even when it exists.

    Returns:
    dict or None: The JSON output of the states metadata. Prints an error message if None.
    """
    global STATES_METADATA_FILENAME
    stored = isfile(STATES_METADATA_FILENAME)
    if force_update or not stored:
        r = get_states_metadata_from_url()
        if not r['success']:
            sys.stderr.write('%s\n' % r['error'])
            return None
        with open(STATES_METADATA_FILENAME, 'w') as local_file:
            json.dump(r['content'], local_file)
        return r['content']
    try:
        with open(STATES_METADATA_FILENAME, 'r') as local_file:
            return json.load(local_file)
    except:
        sys.stderr.write('---- Error while parsing JSON ----\n')
        traceback.print_exc()
        return None
