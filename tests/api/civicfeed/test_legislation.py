import unittest
import requests
import json
from os import remove

from unittest.mock import Mock, patch

from api.civicfeed.civicfeed_key import key
import api.civicfeed.legislation as legislation

class CivicFeedLegislationAPITest(unittest.TestCase):

    @patch('requests.get')
    def test_get_states_metadata_from_url_requests_get_with_correct_arguments(self, mock_method):
        mock_return_dict = Mock()
        mock_return_dict.status_code = 200
        mock_return_dict.headers = {'content-type': 'application/json'}
        mock_return_dict.json = lambda: {}
        mock_method.return_value = mock_return_dict

        expected_url = legislation.STATES_METADATA_URL
        expected_key = key

        actual_dict = legislation.get_states_metadata_from_url()

        mock_method.assert_called_once()
        mock_method.assert_called_with(
                url=expected_url,
                headers={'x-api-key': expected_key})

    @patch('requests.get')
    def test_get_states_metadata_from_url_fails_for_not_200_status_code(self, mock_method):
        mock_return_dict = Mock()
        mock_return_dict.status_code = 404
        mock_method.return_value = mock_return_dict

        expected_dict = {
                'success': False,
                'content': None,
                'error': 'Bad status code: 404'}

        actual_dict = legislation.get_states_metadata_from_url()

        self.assertEqual(expected_dict, actual_dict)

    @patch('requests.get')
    def test_get_states_metadata_from_url_fails_for_not_invalid_content(self, mock_method):
        mock_return_dict = Mock()
        mock_return_dict.status_code = 200
        mock_return_dict.headers = {'content-type': 'text/html'}
        mock_return_dict.text = "test_text"
        mock_method.return_value = mock_return_dict

        expected_dict = {
                'success': False,
                'content': 'test_text',
                'error': 'Not JSON content-type: text/html'}

        actual_dict = legislation.get_states_metadata_from_url()

        self.assertEqual(expected_dict, actual_dict)

    @patch('requests.get')
    def test_get_states_metadata_from_url_returns_mock_successful_query(self, mock_method):
        mock_return_dict = Mock()
        mock_return_dict.status_code = 200
        mock_return_dict.headers = {'content-type': 'application/json; charset=utf-8'}
        mock_return_dict.json = lambda: {'test_key': 'test_value'}
        mock_method.return_value = mock_return_dict

        expected_dict = {
                'success': True,
                'content': {'test_key': 'test_value'},
                'error': None}

        actual_dict = legislation.get_states_metadata_from_url()

        self.assertEqual(expected_dict, actual_dict)

    @patch('api.civicfeed.legislation.get_states_metadata_from_url')
    def test_get_states_metadata_online_and_stores(self, mock_method):
        mock_method.return_value = {
                'success': True,
                'content': {'test_key': 'test_value'},
                'error': None}

        expected_dict = {'test_key': 'test_value'}

        actual_dict = legislation.get_states_metadata()
        actual_dict = legislation.get_states_metadata() # should use stored values
        with open(legislation.STATES_METADATA_FILENAME, 'r') as local_file:
            actual_stored_dict = json.load(local_file)
        remove(legislation.STATES_METADATA_FILENAME)

        mock_method.assert_called_once() # should use stored values
        self.assertEqual(expected_dict, actual_dict)
        self.assertEqual(expected_dict, actual_stored_dict)

    @patch('api.civicfeed.legislation.get_states_metadata_from_url')
    def test_get_states_metadata_force_updates(self, mock_method):
        mock_method.return_value = {
                'success': True,
                'content': {'test_key': 'test_value'},
                'error': None}

        expected_dict = {'test_key': 'test_value'}

        actual_dict = legislation.get_states_metadata()
        actual_dict = legislation.get_states_metadata(force_update=True)
        with open(legislation.STATES_METADATA_FILENAME, 'r') as local_file:
            actual_stored_dict = json.load(local_file)
        remove(legislation.STATES_METADATA_FILENAME)

        self.assertEqual(mock_method.call_count, 2)
        self.assertEqual(expected_dict, actual_dict)
        self.assertEqual(expected_dict, actual_stored_dict)

    @patch('api.civicfeed.legislation.get_states_metadata_from_url')
    def test_get_states_metadata_handles_bad_response(self, mock_method):
        mock_method.return_value = {
                'success': False,
                'content': None,
                'error': 'test_error'}

        expected_dict = None

        actual_dict = legislation.get_states_metadata()

        mock_method.assert_called_once()
        self.assertEqual(expected_dict, actual_dict)

    def test_get_states_metadata_handles_bad_stored_json(self):
        with open(legislation.STATES_METADATA_FILENAME, 'w') as local_file:
            local_file.write("{bad_json_file")
        expected_dict = None

        actual_dict = legislation.get_states_metadata()
        remove(legislation.STATES_METADATA_FILENAME)

        self.assertEqual(expected_dict, actual_dict)
