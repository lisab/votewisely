/* Main Section for Issues in React */

// import React from 'react';

class TexasSection extends React.PureComponent {

  render() {
    return (
        <div id="details">
          <p>{this.props.session}</p>
          <p>{this.props.legislatureName}</p>
          <a href={this.props.legislature}>Legislature</a>
          <p>{this.props.chambers}</p>
          <p>{this.props.seats}</p>
        </div>
    );
  }

}

class IssueList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="issues-list">

        <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <TexasSection
              session="session"
              legislatureName="legislatureName"
              legislature="legislature"
              chambers="chambers"
              seats="seats"
            />
          </div>
        </div>
        <br></br>
      </div>

    );
  }

}

class Purpose extends React.PureComponent {
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">Texas</h2>
      </div>
    );
  }
}

class Main extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <img src="../imgs/texas2.png" id="banner" />
        <Purpose />
        <hr />
        <IssueList />
      </div>
    );
  }
}


ReactDOM.render(
  <Main />,
  document.getElementById('react-texas')
);
