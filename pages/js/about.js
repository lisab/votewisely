/* Main Section for About in React
 * Imports Member Cards: member-card.js
 */

// import React from 'react';

class Member extends React.PureComponent {

  render() {
    return (
      <div id="card" class="container">
        <div class="image-cropper">
          <img src={this.props.image} id="selfie"/>
        </div>
        <div id="details">
          <h2>{this.props.name}</h2>
          <p>{this.props.role}</p>
          <p>{this.props.bio}</p>
        </div>
      </div>
    );
  }

}

class MemberList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="members-list">
        <h2 class="title-text">Meet the Team!</h2>
        <div class="row">
          <div class="col-xs-6 col-md-4 no-padding">
            <Member 
              name="Jesse Martinez"
              role="Front-end Engineer"
              bio="This is my bio"
              image="../imgs/members/jezze.jpg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member 
              name="Lisa Barson"
              role="Front-end Engineer"
              bio="This is my bio"
              image="../imgs/members/lisa.jpeg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Amiti Busgeeth"
              role="Front-end Engineer"
              bio="This is my bio"
              image="../imgs/members/amiti.jpg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Jie Hao Liao"
              role="Full-Stack Engineer"
              bio="This is my bio"
              image="../imgs/members/jay.jpeg"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Fan Yang"
              role="Back-end Engineer"
              bio="This is my bio"
              image="../imgs/members/fan.png"
            />
          </div>
          <div class="col-xs-6 col-md-4 no-padding">
            <Member
              name="Jesus Vasquez"
              role="Back-end Engineer"
              bio="This is my bio"
              image="../imgs/members/jesus.jpg"
            />
          </div>
        </div>

      </div>
    );
  }

}

class Stats extends React.PureComponent {

  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text">The Statistics</h2>
        <div class="container">
          <table class="table table-cover">
            <thead>
              <tr>
                <th scope="col">Names</th>
                <th scope="col">Commits</th>
                <th scope="col">Issues</th>
                <th scope="col">Unit Tests</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

class Purpose extends React.PureComponent {
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">Our Mission</h2>
        <p id="mission-statement"> 
          Our Mission is to help citizens be politically informed 
          and get excited to vote by providing them with unbiased 
          information on politicians and issues based on their location
        </p>
      </div>
    );
  }
}

class Main extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <img src="../imgs/about_banner.jpg" id="banner" />
        <hr />
        <Purpose />
        <hr />
        <MemberList />
        <hr />
        <Stats />
        <div id="links">  
          <a href="https://gitlab.com/votewiselyengineers/votewisely">GitLab Link</a>
        </div>
      </div>
    );
  }
}


ReactDOM.render(
  <Main />, 
  document.getElementById('react-about')
);
