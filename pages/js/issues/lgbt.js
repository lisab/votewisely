/* React.js script on Abortion Issue */

class Banner extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <img src="../imgs/lgbtq_banner.jpg" id="banner"/>
        <h1 id="banner-text">Lesbian, Gay, Bisexual, Transgender, Queer, and more</h1>
      </div>
    )
  }
}

class DescriptionAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute" style={{"margin-top": "120px"}}>
        <h2 class="attribute-title">Description</h2>
        <hr />
        <p>Lesbian, gay, bisexual, transgender, queer, and more (LGBTQ+) rights in the United States of America have significantly progressed over time, with the majority of the progress on LGBT rights having taken place in the late 20th century and early 21st century, however they continue to vary by jurisdiction. Since June 26, 2003, sexual activity between consenting adults of the same sex as well as same-sex adolescents of a close age has been legal nationwide, pursuant to the U.S. Supreme Court ruling in <i>Lawrence v. Texas</i>. As of June 26, 2015, all states license and recognize marriage between same-sex couples as a result of the Supreme Court decision in Obergefell v. Hodges.</p>
        <p>However, the United States has no federal law outlawing discrimination nationwide other than from federal executive orders which have a more limited scope than from protections through federal legislation. This leaves residents of some states unprotected against discrimination in employment, housing, and private or public services. Thus, LGBT persons in the United States may face challenges not experienced by non-LGBT residents.</p>
        <p>The strongest expansions in LGBT rights in the United States have come from the United States Supreme Court. In four landmark rulings between the years 1996 and 2015, the Supreme Court invalidated a state law banning protected class recognition based upon homosexuality, struck down sodomy laws nationwide, struck down Section 3 of the Defense of Marriage Act, and made same-sex marriage legal nationwide.</p>
        <p>LGBT rights-related laws regarding family and anti-discrimination still vary by state. The age of consent in each jurisdiction varies from age 16 to 18, with some jurisdictions maintaining different ages of consent for males/females or for same-sex/opposite-sex relations.</p>
        <p>Twenty-two states plus Washington, D.C. and Puerto Rico outlaw discrimination based on sexual orientation, and twenty states plus Washington, D.C. and Puerto Rico outlaw discrimination based on gender identity or expression. Hate crimes based on sexual orientation or gender identity are also punishable by federal law under the Matthew Shepard and James Byrd, Jr. Hate Crimes Prevention Act of 2009. In 2012 the U.S. Equal Employment Opportunity Commission ruled that Title VII of the Civil Rights Act of 1964 does not allow gender identity-based employment discrimination because it is considered sexual discrimination. In 2015, the U.S. Equal Employment Opportunity Commission concluded that Title VII of the Civil Rights Act of 1964 does not allow sexual orientation discrimination in employment because it is a form of sex discrimination.</p>
        <hr />
        <p class="credits">Credits to <a href="https://en.wikipedia.org/wiki/LGBT_rights_in_the_United_States" target="_blank" class="link">Wikipedia</a> and its <a href="https://en.wikipedia.org/w/api.php" target="_blank" class="link">API</a></p>
      </div>
    )
  }
}

class PublicOpinionAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div class="attribute">
        <h2 class="attribute-title">Public Opinion</h2>
        <hr />
        <p>A March 2014 public opinion poll by Washington Post/ABC News showed support for same-sex marriage at 59% among Americans, and a February 2014 New York Times/CBS News opinion poll showed 56% support for same-sex marriage. A November 2012 Gallup poll indicated 61% support for gays and lesbians being allowed to adopt children.</p>
        <p>The main supporters of LGBT rights in the U.S. have generally been political liberals and libertarians. Regionally, support for the LGBT rights movement has been strongest in the areas of the North and the West coast, and in other states with large urban populations. The national Democratic Party has held the official platform support most initiatives since 2012 for LGBT rights. However, there are some Republican groups advocating for LGBT issues inside the party include the Log Cabin Republicans, GOProud, Young Conservatives for the Freedom To Marry, and College Republicans of the University of Pennsylvania and Columbia University. A poll in March 2014 found that 40% of Republicans support same-sex marriage. In 2013, 52% of Republicans and GOP-leaning independents between the age of 18–49 years old supported same-sex marriage in a joint Washington Post-ABC News poll. A 2014 Pew Forum Poll showed that American Muslims are more likely than Evangelicals to support same-sex marriage 42% to 28%.</p>
        <p>The main opponents of LGBT rights in the U.S. have generally been political and religious conservatives. Conservatives cite various Bible passages from the Old and New Testaments as their justification for opposing LGBT rights. Regionally, LGBT rights opposition has been strongest in the South and in other states with a large rural and conservative population.</p>
        <p>As the movement for same-sex marriage has developed, many national and/or international organizations have opposed that movement. Those organizations include the American Family Association, the Christian Coalition, Family Research Council, Focus on the Family, Save Our Children, NARTH, the national Republican Party, the Roman Catholic Church, The Church of Jesus Christ of Latter-day Saints (LDS Church), the Southern Baptist Convention, Alliance for Marriage, Alliance Defense Fund, Liberty Counsel, and the National Organization for Marriage. A number of these groups have been named as anti-gay hate groups by the Southern Poverty Law Center.</p>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://en.wikipedia.org/wiki/LGBT_rights_in_the_United_States#Public_opinion" target="_blank" class="link">Wikipedia</a> and its <a href="https://en.wikipedia.org/w/api.php" target="_blank" class="link">API</a></p>
        </div>
      </div>
    )
  }
}

class ProConAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">ProCon Views</h2>
        <hr />
        <h3 class="attribute-title" style={{"font-size": "18px"}}>Core Question: Should Gay Marriage Be Legal?</h3>
        <hr />
        <h4 class="attribute-title" style={{"text-align": "left"}}>Why yes?</h4>
        <p>Denying some people the option to marry is discriminatory and creates a second class of citizens. On July 25, 2014 Miami-Dade County Circuit Court Judge Sarah Zabel ruled Florida's gay marriage ban unconstitutional and stated that the ban "serves only to hurt, to discriminate, to deprive same-sex couples and their families of equal dignity, to label and treat them as second-class citizens, and to deem them unworthy of participation in one of the fundamental institutions of our society."  Christine Gregoire, former Washington governor, said in Jan. 2012: "Throughout our history, we have fought discrimination. We have joined together to recognize equality for racial minorities, women, people with disabilities, immigrants... [Legalizing gay marriage] is the right thing to do and it is time." US Seventh Circuit Court of Appeals Judge Richard Posner, in overturning same-sex marriage bans in Wisconsin and Indiana in Sep. 2014, wrote that the bans "discriminate against a minority defined by an immutable characteristic." As well as discrimination based on sexual orientation, gay marriage bans discriminate based on one's sex. As explained by David S. Cohen, JD, Associate Professor at the Drexel University School of Law, "Imagine three people—Nancy, Bill, and Tom... Nancy, a woman, can marry Tom, but Bill, a man, cannot... Nancy can do something (marry Tom) that Bill cannot, simply because Nancy is a woman and Bill is a man."</p>
        <p>Same-sex couples should have access to the same benefits enjoyed by heterosexual married couples. There are 1,138 benefits, rights and protections available to married couples in federal law alone, according to a General Accounting Office assessment made in 2004. Benefits only available to married couples include hospital visitation during an illness, the option of filing a joint tax return to reduce a tax burden, access to family health coverage, US residency and family unification for partners from another country, and bereavement leave and inheritance rights if a partner dies. Married couples also have access to protections if the relationship ends, such as child custody, spousal or child support, and an equitable division of property. Married couples in the US armed forces are offered health insurance and other benefits unavailable to domestic partners. The Internal Revenue Service (IRS) and the US Department of Labor also recognize married couples, for the purpose of granting tax, retirement and health insurance benefits. The US federal government does not grant equivalent benefits to gay couples in civil unions or domestic partnerships. An Oct. 2, 2009 analysis by the New York Times estimated that same-sex couples denied marriage benefits will incur an additional $41,196 to $467,562 in expenses over their lifetimes compared with married heterosexual couples. A Jan. 2014 analysis published by the Atlantic concluded that unmarried women pay up to one million dollars more over their lifetimes than married women for healthcare, taxes, and other expenses.</p>
        <hr />
        <h4 class="attribute-title" style={{"text-align": "left"}}>Why not?</h4>
        <p>The institution of marriage has traditionally been defined as being between a man and a woman. In upholding gay marriage bans in Kentucky, Michigan, Ohio and Tennessee on Nov. 6, 2014, 6th US District Court of Appeals Judge Jeffrey S. Sutton wrote that "marriage has long been a social institution defined by relationships between men and women. So long defined, the tradition is measured in millennia, not centuries or decades. So widely shared, the tradition until recently had been adopted by all governments and major religions of the world." In the Oct. 15, 1971 decision <i>Baker v. Nelson</i>, the Supreme Court of Minnesota found that "the institution of marriage as a union of man and woman, uniquely involving the procreation and rearing of children within a family, is as old as the book of Genesis." John F. Harvey, MA, STL, late Catholic priest, wrote in July 2009 that "Throughout the history of the human race the institution of marriage has been understood as the complete spiritual and bodily communion of one man and one woman."</p>
        <p>Marriage is for procreation and should not be extended to same-sex couples because they cannot produce children together. Allowing gay marriage would only further shift the purpose of marriage from producing and raising children to adult gratification. A California Supreme Court ruling from 1859 stated that "the first purpose of matrimony, by the laws of nature and society, is procreation." Nobel Prize-winning philosopher Bertrand Russell stated that "it is through children alone that sexual relations become important to society, and worthy to be taken cognizance of by a legal institution." Court papers filed in July 2014 by attorneys defending Arizona's gay marriage ban stated that "the State regulates marriage for the primary purpose of channeling potentially procreative sexual relationships into enduring unions for the sake of joining children to both their mother and their father... Same-sex couples can never provide a child with both her biological mother and her biological father." Contrary to the pro gay marriage argument that some different-sex couples cannot have children or don't want them, even in those cases there is still the potential to produce children. Seemingly infertile heterosexual couples sometimes produce children, and medical advances may allow others to procreate in the future. Heterosexual couples who do not wish to have children are still biologically capable of having them, and may change their minds.</p>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://gaymarriage.procon.org/" target="_blank" class="link">ProCon.org</a></p>
        </div>
      </div>
    )
  }
}

class StatementsAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">Recent Congressional Statements</h2>
        <hr />
        <ul>
          <li>
            <a href="https://www.menendez.senate.gov/news-and-events/press/menendez-booker-colleagues-introduce-bill-to-protect-civil-rights-for-all-americans" target="_blank" class="link">Menendez, Booker, Colleagues Introduce Bill to Protect Civil Rights for All Americans</a> by Robert Menendez (Democrat) on February 28th, 2019 in NJ.
          </li>
          <li>
            <a href="https://www.murray.senate.gov/public/index.cfm/newsreleases?ContentRecord_id=FF5FF711-10D2-43BF-999C-9784EF2C7590" target="_blank" class="link">Senator Murray Delivers Keynote Speech Outlining Need for Bold, Federal Action in Higher Education to Help Students Succeed</a> by Patty Murray (Democrat) on February 28th, 2019 in WA.
          </li>
          <li>
            <a href="https://bobbyscott.house.gov/media-center/press-releases/scott-kennedy-harris-reintroduce-bill-to-protect-individuals-from" target="_blank" class="link">Scott, Kennedy, Harris Reintroduce Bill to Protect Individuals from Discrimination</a> by Robert C. Scott (Democrat) on February 28th, 2019 in VA.
          </li>
          <li>
            <a href="https://www.harris.senate.gov/news/press-releases/harris-reintroduces-legislation-to-protect-civil-rights-of-all-americans" target="_blank" class="link">Harris Reintroduces Legislation to Protect Civil Rights of All Americans</a> by Kamala Harris (Democrat) on February 28th, 2019 in CA.
          </li>
          <li>
            <a href="https://haaland.house.gov/media/press-releases/haaland-meets-transgender-troops-ahead-subcommittee-hearing" target="_blank" class="link">Haaland Meets with Transgender Troops Ahead of Subcommittee Hearing</a> by Debra Haaland (Democrat) on February 28th, 2019 in NM.
          </li>
        </ul>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
        </div>
      </div>
    )
  }
}

class BillsAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">Related Congressional Bills</h2>
        <hr />
        <ul>
          <li>
            <a href="https://www.congress.gov/115/bills/s1114/BILLS-115s1114is.pdf" target="_blank" class="link">S.1114 - A bill to nullify the effect of the recent Executive order laying a foundation for discrimination against LGBTQ individuals, women, religious minorities, and others under the pretext of religious freedom.</a> [<a href="https://www.congress.gov/bill/115th-congress/senate-bill/1114/" target="_blank" class="link">Congress.gov</a>] by Dianne Feinstein.
          </li>
          <li>
            <a href="https://www.congress.gov/115/bills/sres398/BILLS-115sres398ats.pdf" target="_blank" class="link">S.Res.398 - A resolution supporting the observation of "National Girls & Women in Sports Day" on February 7, 2018, to raise awareness of and celebrate the achievements of girls and women in sports.</a> [<a href="https://www.congress.gov/bill/115th-congress/senate-resolution/398/" target="_blank" class="link">Congress.gov</a>] by Dianne Feinstein.
          </li>
          <li>
            <a href="https://www.congress.gov/115/bills/s1580/BILLS-115s1580enr.pdf" target="_blank" class="link">S.1580 - Protecting Girls' Access to Education in Vulnerable Settings Act</a> [<a href="https://www.congress.gov/bill/115th-congress/senate-bill/1580/" target="_blank" class="link">Congress.gov</a>] by Marco Rubio.
          </li>
          <li>
            <a href="https://www.congress.gov/114/bills/s1080/BILLS-114s1080is.pdf" target="_blank" class="link">S.1080 - Protect Marriage from the Courts Act of 2015</a> [<a href="https://www.congress.gov/bill/114th-congress/senate-bill/1080/" target="_blank" class="link">Congress.gov</a>] by Ted Cruz.
          </li>
        </ul>
        <hr />
        <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
      </div>
    )
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Banner />
        <DescriptionAttribute />
        <PublicOpinionAttribute />
        <ProConAttribute />
        <StatementsAttribute />
        <BillsAttribute />
      </div>
    )
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('lgbt-component')
);
