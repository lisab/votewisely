/* React.js script on Abortion Issue */

class Banner extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <img src="../imgs/abortion_banner.png" id="banner"/>
        <h1 id="banner-text">Abortion</h1>
      </div>
    )
  }
}

class DescriptionAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute" style={{"margin-top": "120px"}}>
        <h2 class="attribute-title">Description</h2>
        <hr />
        <p>Abortion in the United States is among the most controversial and divisive issues in United States culture and politics. Various anti-abortion laws have been in force in each state since at least 1900.</p>
        <p>Before the U.S. Supreme Court decision <i>Roe v. Wade</i> decriminalised abortion nationwide in 1973, abortion was already legal in several states, but the decision imposed a uniform framework for state legislation on the subject. It established a minimal period during which abortion is legal (with more or fewer restrictions throughout the pregnancy). That basic framework, modified in <i>Planned Parenthood v. Casey</i> (1992), remains nominally in place, although the effective availability of abortion varies significantly from state to state, as many counties have no abortion providers. <i>Planned Parenthood v. Casey</i> held that a law cannot place legal restrictions imposing an undue burden for "the purpose or effect of placing a substantial obstacle in the path of a woman seeking an abortion of a nonviable fetus."</p>
        <p>The abortion debate most commonly relates to the "induced abortion" of an embryo or fetus at some point in a pregnancy, which is also how the term is used in a legal sense. Some also use the term "elective abortion", which is used in relation to a claim to an unrestricted right of a woman to an abortion, whether or not she chooses to have one.</p>
        <p>In medical parlance, "abortion" can refer to either miscarriage or abortion until the fetus is viable. After viability, doctors call an abortion a "termination of pregnancy".</p>
        <hr />
        <p class="credits">Credits to <a href="https://en.wikipedia.org/wiki/Abortion_in_the_United_States" target="_blank" class="link">Wikipedia</a> and its <a href="https://en.wikipedia.org/w/api.php" target="_blank" class="link">API</a></p>
      </div>
    )
  }
}

class PublicOpinionAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div class="attribute">
        <h2 class="attribute-title">Public Opinion</h2>
        <hr />
        <p>Leading up to the 40th anniversary of the <i>Roe v. Wade</i> Supreme Court decision in January 2013, a majority of Americans believed abortion should be legal in all or most cases, according to a poll by NBC News and the Wall Street Journal. As well, approximately 70% of respondents oppose <i>Roe v. Wade</i> being overturned, which is the highest percentage on this question since 1989. A poll by the Pew Research Center yielded similar results. Moreover, 48% of Republicans opposed overturning Roe, compared to 46% who supported overturning it.</p>
        <p>In the United States, the main actors in the abortion debate are most often labeled either as "pro-choice" or "pro-life", though shades of opinion exist, and most Americans are considered to be somewhere in the middle. A 2018 Gallup survey found the percentages that were pro-choice or pro-life were equal (at 48%), but more people considered abortion morally wrong (48%) than morally acceptable (43%). The poll results also indicated that Americans harbor a diverse and shifting set of opinions on the legal status of abortion in the United States. The survey found that only 29% of respondents believed abortion should be legal in all circumstances, and 50% of respondents believed that abortion should be legal under certain circumstances. Recent polling results also found that only 34% of Americans were satisfied with abortion laws in the United States.</p>
        <p>Gallup notes that abortion attitudes are shifting. Gallup declared in May 2010 that more Americans identifying as "pro-life" is "the new normal", while also noting that there had been no increase in opposition to abortion. It suggested that political polarization may have prompted more Republicans to call themselves "pro-life". The terms "pro-choice" and "pro-life" do not always reflect a political view or fall along a binary; in one Public Religion Research Institute poll, seven in ten Americans described themselves as "pro-choice" while almost two-thirds described themselves as "pro-life". The same poll found that 56% of Americans were in favor of legal access to abortion in all or some cases.</p>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://en.wikipedia.org/wiki/Abortion_in_the_United_States#Public_opinion" target="_blank" class="link">Wikipedia</a> and its <a href="https://en.wikipedia.org/w/api.php" target="_blank" class="link">API</a></p>
        </div>
      </div>
    )
  }
}

class ProConAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">ProCon Views</h2>
        <hr />
        <h3 class="attribute-title" style={{"font-size": "18px"}}>Core Question: Should Abortion Be Legal?
?</h3>
        <hr />
        <h4 class="attribute-title" style={{"text-align": "left"}}>Why yes?</h4>
        <p>The US Supreme Court has declared abortion to be a "fundamental right" guaranteed by the US Constitution. The landmark abortion case <i>Roe v. Wade</i>, decided on Jan. 22, 1973 in favor of abortion rights, remains the law of the land. The 7-2 decision stated that the Constitution gives "a guarantee of certain areas or zones of privacy," and that "This right of privacy... is broad enough to encompass a woman's decision whether or not to terminate her pregnancy."</p>
        <p>Reproductive choice empowers women by giving them control over their own bodies. The choice over when and whether to have children is central to a woman's independence and ability to determine her future. Former Supreme Court Justice Sandra Day O'Connor wrote in the 1992 decision in <i>Planned Parenthood v. Casey</i>, "The ability of women to participate equally in the economic and social life of the Nation has been facilitated by their ability to control their reproductive lives." Supreme Court Justice Ruth Bader Ginsburg wrote in her dissenting opinion in <i>Gonzales v. Carhart</i> (2007) that undue restrictions on abortion infringe upon "a woman's autonomy to determine her life's course, and thus to enjoy equal citizenship stature." CNN senior legal analyst Jeffrey Toobin, JD, stated that <i>Roe v. Wade</i> was "a landmark of what is, in the truest sense, women's liberation."</p>
        <hr />
        <h4 class="attribute-title" style={{"text-align": "left"}}>Why not?</h4>
        <p>Abortion is murder. The killing of an innocent human being is wrong, even if that human being has yet to be born. Unborn babies are considered human beings by the US government. The federal <i>Unborn Victims of Violence Act</i>, which was enacted "to protect unborn children from assault and murder," states that under federal law, anybody intentionally killing or attempting to kill an unborn child should "be punished... for intentionally killing or attempting to kill a human being." The act also states that an unborn child is a "member of the species homo sapiens." At least 38 states have passed similar fetal homicide laws.</p>
        <p>Life begins at conception, so unborn babies are human beings with a right to life. Upon fertilization, a human individual is created with a unique genetic identity that remains unchanged throughout his or her life. This individual has a fundamental right to life, which must be protected. Jerome Lejeune, the French geneticist who discovered the chromosome abnormality that causes Down syndrome, stated that "To accept the fact that after fertilization has taken place a new human has come into being is no longer a matter of taste or opinion... The human nature of the human being from conception to old age is not a metaphysical contention, it is plain experimental evidence."</p>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://abortion.procon.org/" target="_blank" class="link">ProCon.org</a></p>
        </div>
      </div>
    )
  }
}

class StatementsAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">Recent Congressional Statements</h2>
        <hr />
        <ul>
          <li>
            <a href="https://www.murray.senate.gov/public/index.cfm/newsreleases?ContentRecord_id=0E72602B-E582-4A13-919F-87837B1A2B85" target="_blank" class="link">Senator Murray Reintroduces Bill to Stop Trump Administration from Detaining and Shackling Pregnant Women</a> by Patty Murray (Democrat) on March 5th, 2019 in WA.
          </li>
          <li>
            <a href="https://schweikert.house.gov/media-center/press-releases/congressman-david-schweikert-awarded-family-research-councils-true-blue" target="_blank" class="link">Congressman David Schweikert Awarded Family Research Council's True Blue Award</a> by David Schweikert (Republican) on March 4th, 2019 in AZ.
          </li>
          <li>
            <a href="https://scalise.house.gov/press-release/scalise-inhofe-where-%E2%80%98born-alive%E2%80%99-abortion-bill-goes-here" target="_blank" class="link">Scalise, Inhofe: Where the Born Alive abortion bill goes from here</a> by Steve Scalise (Republican) on March 4th, 2019 in LA.
          </li>
          <li>
            <a href="https://roby.house.gov/newsroom/in-the-news/alabama-political-reporter-roby-and-palmer-voice-support-born-live-legislation" target="_blank" class="link">Alabama Political Reporter: Roby and Palmer voice support for born live legislation</a> by Martha Roby (Republican) on March 4th, 2019 in AL.
          </li>
          <li>
            <a href="https://www.ernst.senate.gov//public/index.cfm/in-the-news?ContentRecord_id=110D8EA8-9F93-417A-A403-00175C5E6DE9" target="_blank" class="link">KIOW: Ernst Votes to Protect Lives of Children Born After Failed Abortions</a> by Joni Ernst (Republican) on March 4th, 2019 in IA.
          </li>
        </ul>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
        </div>
      </div>
    )
  }
}

class BillsAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">Related Congressional Bills</h2>
        <hr />
        <ul>
          <li>
            <a href="https://www.congress.gov/108/bills/s2219/BILLS-108s2219is.pdf" target="_blank" class="link">S.2219 - Motherhood Protection Act</a> [<a href="https://www.congress.gov/bill/108th-congress/senate-bill/2219/" target="_blank" class="link">Congress.gov</a>] by Dianne Feinstein.
          </li>
          <li>
            <a href="https://www.congress.gov/116/bills/s119/BILLS-116s119is.pdf" target="_blank" class="link">S.119 - Child Interstate Abortion Notification Act</a> [<a href="https://www.congress.gov/bill/116th-congress/senate-bill/119/" target="_blank" class="link">Congress.gov</a>] by Marco Rubio.
          </li>
          <li>
            <a href="https://www.congress.gov/114/plaws/publ22/PLAW-114publ22.pdf" target="_blank" class="link">S.178 - Justice for Victims of Trafficking Act of 2015</a> [<a href="https://www.congress.gov/bill/114th-congress/senate-bill/178/" target="_blank" class="link">Congress.gov</a>] by Ted Cruz.
          </li>
        </ul>
        <hr />
        <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
      </div>
    )
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Banner />
        <DescriptionAttribute />
        <PublicOpinionAttribute />
        <ProConAttribute />
        <StatementsAttribute />
        <BillsAttribute />
      </div>
    )
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('abortion-component')
);
