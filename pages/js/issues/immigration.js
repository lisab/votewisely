/* React.js script on Immigration Issue */

class Banner extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <img src="https://upload.wikimedia.org/wikipedia/commons/9/9a/Welcome_to_the_land_of_freedom.png" id="banner"/>
        <h1 id="banner-text">Immigration</h1>
      </div>
    )
  }
}

class DescriptionAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute" style={{"margin-top": "120px"}}>
        <h2 class="attribute-title">Description</h2>
        <hr />
        <p>
          Immigration to the United States is the international movement of non-U.S. nationals in order to reside permanently in the country. Lawful immigration has been a major source of population growth and cultural change throughout much of the U.S. history. Because the United States is a settler colonial society, all Americans, with the exception of the small percent of Native Americans, can trace their ancestry to immigrants from other nations around the world.
        </p>
        <p>
          The economic, social, and political aspects of immigration have caused controversy regarding such issues as maintaining ethnic homogeneity, workers for employers versus jobs for non-immigrants, settlement patterns, impact on upward social mobility, crime, and voting behavior.
        </p>
        <hr />
        <p class="credits">Credits to <a href="https://en.wikipedia.org/wiki/Immigration_to_the_United_States" target="_blank" class="link">Wikipedia</a> and its <a href="https://en.wikipedia.org/w/api.php" target="_blank" class="link">API</a></p>
      </div>
    )
  }
}

class PublicOpinionAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div class="attribute">
        <h2 class="attribute-title">Public Opinion</h2>
        <hr />
        <p>
          American opinion regarding how immigrants affect the country and how the government should respond to illegal immigration have changed over time. In 2006, out of all U.S. adults surveyed, 28% declared that they believed the growing number of immigrants helped American workers and 55% believed that it hurt American workers. In 2016, those views had changed, with 42% believing that they helped and 45% believing that they hurt. The PRRI 2015 American Values Atlas showed that between 46% and 53% of Americans believed that "the growing number of newcomers from other countries ... strengthens American society." In the same year, 57% and 66% of Americans chose that the U.S. should "allow [immigrants living in the U.S. illegally] a way to become citizens provided they meet certain requirements."
        </p>
        <p>
          In February 2017, the American Enterprise Institute released a report on recent surveys about immigration issues. In July 2016, 63% of Americans favored the temporary bans of immigrants from areas with high levels of terrorism and 53% said the U.S. should allow fewer refugees to enter the country. In November 2016, 55% of Americans were opposed to building a border wall with Mexico. Since 1994, Pew Research center has tracked a change from 63% of Americans saying that immigrants are a burden on the country to 27%.
        </p>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://en.wikipedia.org/wiki/Immigration_to_the_United_States#Public_opinion" target="_blank" class="link">Wikipedia</a> and its <a href="https://en.wikipedia.org/w/api.php" target="_blank" class="link">API</a></p>
        </div>
      </div>
    )
  }
}

class ProConAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">ProCon Views</h2>
        <hr />
        <h3 class="attribute-title" style={{"font-size": "18px"}}>Core Question: Should the Government Allow Immigrants Who Are Here Illegally to Become US Citizens?</h3>
        <hr />
        <h4 class="attribute-title" style={{"text-align": "left"}}>Why yes?</h4>
        <p>Barack Obama, 44th US President, in remarks at Del Sol High School in Las Vegas, Nevada on Jan. 29, 2013, available at whitehouse.gov, stated:</p>
        <p>"[W]e have to deal with the 11 million individuals who are here illegally. We all agree that these men and women should have to earn their way to citizenship. But for comprehensive immigration reform to work, it must be clear from the outset that there is a pathway to citizenship...</p>
        <p>We've got to lay out a path -- a process that includes passing a background check, paying taxes, paying a penalty, learning English, and then going to the back of the line, behind all the folks who are trying to come here legally. That's only fair, right?...</p>
        <p>So that means it won't be a quick process but it will be a fair process. And it will lift these individuals out of the shadows and give them a chance to earn their way to a green card and eventually to citizenship."</p>
        <hr />
        <h4 class="attribute-title" style={{"text-align": "left"}}>Why not?</h4>
        <p>Ashley Nunes, PhD, Research Scientist at Massachusetts Institute of Technology's Center for Transportation and Logistics, in a Mar. 31, 2017 article, "A Path to Legal Status but Not Citizenship," available at the National Review website, stated:</p>
        <p>"A more pragmatic solution would be to offer a path to legalization that stops short of citizenship. That would meet the humanitarian imperative to keep families together. But it would also hold those who have violated immigration laws accountable for their actions. This would apply only to undocumented workers who were of legal age when they entered the United States; those who were not of legal age should be given a citizenship path identical to the one that is available to legal immigrants.</p>
        <p>Except for those who were born on American soil, citizenship is not a right. It's a privilege. A path short of citizenship sends a powerful message to America's legal-immigrant community, whose members have worked tirelessly to follow existing immigration guidelines. There is a rule of law, and citizenship is granted to those who follow it."</p>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://immigration.procon.org/view.answers.php?questionID=001362" target="_blank" class="link">ProCon.org</a></p>
        </div>
      </div>
    )
  }
}

class StatementsAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">Recent Congressional Statements</h2>
        <hr />
        <ul>
          <li>
            <a href="https://desaulnier.house.gov/media-center/desaulnier-digest/history-home-and-washington" target="_blank" class="link">History at Home and in Washington</a> by Mark DeSaulnier (Republican) on March 4th, 2019 in CA.
          </li>
          <li>
            <a href="https://www.paul.senate.gov/news/dr-rand-paul-introduces-%E2%80%98be-safe%E2%80%99-act-fund-border-security" target="_blank" class="link">Dr. Rand Paul Introduces ‘BE SAFE’ Act to Fund Border Security</a> by Rand Paul (Republican) on March 4th, 2019 in KY.
          </li>
          <li>
            <a href="https://www.tillis.senate.gov//public/index.cfm/press-releases?ContentRecord_id=68EC1CE8-87F2-4C3D-A306-1372BEB87B1C" target="_blank" class="link">Tillis, Colleagues Introduce Legislation Preventing Dangerous Criminals with Known Gang Associations From Entering the U.S.</a> by Thom Tillis (Republican) on March 4th, 2019 in NC.
          </li>
          <li>
            <a href="http://flores.house.gov/news/documentsingle.aspx?DocumentID=399184" target="_blank" class="link">The Eagle: President is right to declare a national emergency</a> by Bill Flores (Republican) on March 3rd, 2019 in TX.
          </li>
          <li>
            <a href="https://butterfield.house.gov/media-center/press-releases/nc-democratic-delegation-demands-answers-from-ice-on-aggressive-raids-in" target="_blank" class="link">NC Democratic Delegation Demands Answers from ICE on Aggressive Raids in North Carolina</a> by G. K. Butterfield (Democrat) on March 1st, 2019 in NC.
          </li>
        </ul>
        <hr />
        <div>
          <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
        </div>
      </div>
    )
  }
}

class BillsAttribute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="attribute">
        <h2 class="attribute-title">Related Congressional Bills</h2>
        <hr />
        <ul>
          <li>
            <a href="https://www.congress.gov/116/bills/s25/BILLS-116s25is.pdf" target="_blank" class="link">S.25 - EL CHAPO Act</a> [<a href="https://www.congress.gov/bill/116th-congress/senate-bill/25/" target="_blank" class="link">Congress.gov</a>] by Ted Cruz.
          </li>
          <li>
            <a href="https://www.congress.gov/116/bills/s292/BILLS-116s292is.pdf" target="_blank" class="link">S.292 - Keep Families Together Act</a> [<a href="https://www.congress.gov/bill/116th-congress/senate-bill/292/" target="_blank" class="link">Congress.gov</a>] by Dianne Feinstein.
          </li>
          <li>
            <a href="https://www.congress.gov/116/bills/s388/BILLS-116s388is.pdf" target="_blank" class="link">S.388 - Families, Not Facilities Act of 2019</a> [<a href="https://www.congress.gov/bill/116th-congress/senate-bill/388/" target="_blank" class="link">Congress.gov</a>] by Kalama D. Harris.
          </li>
          <li>
            <a href="https://www.congress.gov/115/bills/s942/BILLS-115s942is.pdf" target="_blank" class="link">S.942 - Counterterrorism Screening and Assistance Act of 2017</a> [<a href="https://www.congress.gov/bill/115th-congress/senate-bill/942/" target="_blank" class="link">Congress.gov</a>] by Marco Rubio.
          </li>
        </ul>
        <hr />
        <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
      </div>
    )
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Banner />
        <DescriptionAttribute />
        <PublicOpinionAttribute />
        <ProConAttribute />
        <StatementsAttribute />
        <BillsAttribute />
      </div>
    )
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('immigration-component')
);
